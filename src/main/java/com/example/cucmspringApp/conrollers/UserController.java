package com.example.cucmspringApp.conrollers;

import com.example.cucmspringApp.handlers.UsernameAlreadyExistException;
import com.example.cucmspringApp.models.CucmUser;
import com.example.cucmspringApp.models.User;
import com.example.cucmspringApp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('user:read')")
    @GetMapping(value="")
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    @PreAuthorize("hasAuthority('user:read')")
    @GetMapping(value = "/{username}")
    public Optional<User> getUser(@PathVariable String username){
        return userService.getUserByUsername(username);
    }

//    @PreAuthorize("permitAll()")
    @PostMapping(value="")
    public ResponseEntity<?> addUser(@RequestBody User user) {
        try {
            return new ResponseEntity(userService.addUser(user), HttpStatus.OK);
        }catch(UsernameAlreadyExistException ex){
            return new ResponseEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }catch(IllegalArgumentException ex){
            return new ResponseEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('user:write')")
    @PutMapping("/activataccount/{username}")
    public ResponseEntity<String> activateUserAcc(@PathVariable String username){
        try{
            userService.activateUserAcc(username);
            return new ResponseEntity("user account with username : " + username + "has been activated now", HttpStatus.OK);
        }catch(UsernameNotFoundException ex){
            return new ResponseEntity("sorry -_- cant find user !!", HttpStatus.BAD_REQUEST);
        }
    }
}
