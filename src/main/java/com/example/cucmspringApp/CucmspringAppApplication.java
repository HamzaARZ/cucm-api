package com.example.cucmspringApp;

import com.example.cucmspringApp.models.User;
import com.example.cucmspringApp.models.jwtAuth.UserRole;
import com.example.cucmspringApp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class CucmspringAppApplication {
	@Autowired
	private static UserRepository userRepository;

	public static void main(String[] args) {





		SpringApplication.run(CucmspringAppApplication.class, args);
	}

}
