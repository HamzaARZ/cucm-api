package com.example.cucmspringApp.services;

import com.example.cucmspringApp.models.CucmUser;
import com.example.cucmspringApp.repositories.CucmUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CucmUserService {
    @Autowired
    private CucmUserRepository cucmUserRepository;


    public List<CucmUser> getCucmUsers() {
        List<CucmUser> cucmUsers = new ArrayList<>();
        cucmUserRepository.findAll().forEach(cucmUsers::add);
        return cucmUsers;
    }


    public CucmUser getCucmUserById(Long id) throws NoSuchElementException {
        return cucmUserRepository.findById(id).get();
    }


    public CucmUser addCucmUser(CucmUser cucmUser){
        return cucmUserRepository.save(cucmUser);
    }


    public CucmUser updateCucmUser(CucmUser cucmUser) {
        return cucmUserRepository.save(cucmUser);
    }

    public void deleteCucmUser(Long id) throws EmptyResultDataAccessException {
        cucmUserRepository.deleteById(id);
    }

    public Boolean isExist(Long id) {
        return cucmUserRepository.existsById(id);
    }
}
