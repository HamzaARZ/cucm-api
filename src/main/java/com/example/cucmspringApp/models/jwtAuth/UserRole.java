package com.example.cucmspringApp.models.jwtAuth;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import static com.example.cucmspringApp.models.jwtAuth.UserPermission.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum UserRole {
    ADMIN(Arrays.asList(
            CUCM_USER_READ, CUCM_USER_WRITE,
            USER_READ, USER_WRITE
    )),

    MANAGER(Arrays.asList(
            CUCM_USER_READ, CUCM_USER_WRITE,
            USER_READ
    )),
    USER(Arrays.asList(
            CUCM_USER_READ,
            USER_READ
    ));


    private final List<UserPermission> permissions;

    UserRole(List<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public List<UserPermission> getPermissions() {
        return permissions;
    }

    public List<SimpleGrantedAuthority> getGrantedAuthorities() {
        List<SimpleGrantedAuthority> permissions = getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toList());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
