package com.example.cucmspringApp.models;


import javax.persistence.*;
@Entity
public class CucmUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long cucmUserId;
    private String password;
    private String name;
    private String local;

    public CucmUser() {
    }

    public CucmUser(Long cucmUserId, String password, String name, String local) {
        this.cucmUserId= cucmUserId;
        this.password = password;
        this.name = name;
        this.local = local;
    }

    public Long getCucmUserId() {
        return cucmUserId;
    }

    public void setCucmUserId(Long cucmUserId) {
        this.cucmUserId = cucmUserId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    @Override
    public String toString() {
        return "CucmUser{" +
                "cucmUserId=" + cucmUserId +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", local='" + local + '\'' +
                '}';
    }
}