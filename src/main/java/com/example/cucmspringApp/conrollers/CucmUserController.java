package com.example.cucmspringApp.conrollers;

import com.example.cucmspringApp.models.CucmUser;
import com.example.cucmspringApp.services.CucmUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/cucmusers")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CucmUserController {

    @Autowired
    private CucmUserService cucmUserService;

//    @PreAuthorize("hasAuthority('cucmUser:read')")
    @GetMapping(value="")
    public List<CucmUser> getCucmUsers(){
        return cucmUserService.getCucmUsers();
    }

    @PreAuthorize("hasAuthority('cucmUser:read')")
    @GetMapping(value="/{id}")
    public ResponseEntity<?> getCucmUserByID(@PathVariable Long id){
        try{
            return new ResponseEntity(cucmUserService.getCucmUserById(id), HttpStatus.OK);
        }catch (NoSuchElementException ex){
            System.out.println("no user found !!");
            return new ResponseEntity("no user found with id : " + id, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PreAuthorize("hasAuthority('cucmUser:write')")
    @PostMapping(value="")
    public CucmUser addCucmUser(@RequestBody CucmUser cucmUser){
        return cucmUserService.addCucmUser(cucmUser);
    }

    @PreAuthorize("hasAuthority('cucmUser:write')")
    @PutMapping(value = "")
    public CucmUser updateCucmUser(@RequestBody CucmUser cucmUser){
        return cucmUserService.updateCucmUser(cucmUser);
    }


    @PreAuthorize("hasAuthority('cucmUser:write')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteCucmUser(@PathVariable Long id){
        try {
            cucmUserService.deleteCucmUser(id);
            return new ResponseEntity("user with id : '" + id + "' has been deleted." , HttpStatus.OK);
        }catch (EmptyResultDataAccessException ex){
            return new ResponseEntity("no user found with id : " + id , HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



}
