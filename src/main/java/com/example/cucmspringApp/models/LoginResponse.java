package com.example.cucmspringApp.models;

public class LoginResponse {
    private Boolean success;
    private String message;
    private String token;

    public LoginResponse() {
    }

    public LoginResponse(Boolean success, String message, String token) {
        this.success = success;
        this.message = message;
        this.token = token;
    }

    public Boolean getsuccess() {
        return success;
    }

    public void setsuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
