package com.example.cucmspringApp.conrollers;

import com.example.cucmspringApp.models.LoginResponse;
import com.example.cucmspringApp.models.jwtAuth.LoginRequest;
import com.example.cucmspringApp.services.security.JwtService;
import com.example.cucmspringApp.services.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @PostMapping(value="/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest loginRequest) throws Exception {

        try{
            authenticate(loginRequest.getUsername(), loginRequest.getPassword());
            final UserDetails userDetails = userDetailsService
                    .loadUserByUsername(loginRequest.getUsername());

            final String token = jwtService.generateToken(userDetails);

            //return ResponseEntity.ok().header("authorization", token).build();
            return new ResponseEntity(new LoginResponse(true, "login successfuly", token), HttpStatus.OK);
        }catch(Exception ex) {
            return new ResponseEntity( new LoginResponse(false, "invalide login", null), HttpStatus.BAD_REQUEST);
        }

    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {

            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            System.out.println("INVALID_CREDENTIALS : " + username + " " + password);
            throw new Exception("INVALID_CREDENTIALS", e);

        }
    }
}
