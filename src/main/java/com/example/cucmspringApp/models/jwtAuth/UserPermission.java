package com.example.cucmspringApp.models.jwtAuth;

public enum UserPermission {
    CUCM_USER_READ("cucmUser:read"),
    CUCM_USER_WRITE("cucmUser:write"),
    USER_READ("user:read"),
    USER_WRITE("user:write");

    private final String permission;


    UserPermission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
